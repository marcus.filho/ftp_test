package br.com.sicoob;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPListParseEngine;
import org.apache.commons.net.ftp.FTPReply;

public class FTPTest {

	public static void main(String[] args) {
		final FTPClientConfig config = new FTPClientConfig();

		config.setUnparseableEntries(false);
		// com proxy: ftp = new FTPHTTPClient(proxyHost, proxyPort, proxyUser, proxyPassword);
		FTPClient ftp = new FTPClient();
		ftp.configure(config);
		ftp.setControlKeepAliveTimeout(300); // set timeout to 5 minutes

		boolean error = false;
		try {
			int reply;
			String server = "ftp.cetip.com.br";
			ftp.setConnectTimeout(20000);
			ftp.connect(server);
			System.out.println("Connected to " + server + ".");
			System.out.print(ftp.getReplyString());
			ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));

			reply = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				System.err.println("FTP server refused connection.");
				System.exit(1);
			}
			if (!ftp.login("anonymous", "")) {
				ftp.logout();
				System.err.println("Login failed.");
				System.exit(1);
			}
			//ftp.setBufferSize(1024 * 1024);
			ftp.setAutodetectUTF8(true);
			ftp.setFileType(FTP.ASCII_FILE_TYPE);
			ftp.enterLocalActiveMode();
			// ftp.enterLocalPassiveMode();
			ftp.setUseEPSVwithIPv4(true);
			ftp.printWorkingDirectory();
			ftp.changeWorkingDirectory("/Public/");
			FTPFile[] files = ftp.listFiles();
			for (FTPFile f : files) {
				System.out.println(f.getName());
			}
			ftp.logout();
		} catch (IOException e) {
			error = true;
			e.printStackTrace();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
			System.exit(error ? 1 : 0);
		}
	}
}
